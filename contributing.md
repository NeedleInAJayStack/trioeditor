# Structure

This repo contains the editor plugin (trioeditor-editor) and the feature (trioeditor-feature). The feature is
used to package the plugin for installation into an Eclipse installation.

# Setup

* Clone the [repo](https://gitlab.com/NeedleInAJayStack/trioeditor)
* Open an eclipse instance with RCP support or F4. This should provide the IDE with plugin development tools.
* Import each directory as an Eclipse project.

# Run

You can run the application by right-clicking on the editor root folder and selecting `Run as` -> `Eclipse Application`.
This will open a new instance of Eclipse with the plugin installed. Of course, you need to open a trio file to see the 
formatting, so an example file is provided in the `trioeditor-editor/res/test/` directory.

# Release

Make sure the version numbers of all plugins and features are updated prior to release.

To export an installable:

* Right click the `trioeditor-feature` project, and click `Export...`
* Select `Plug-in Development` -> `Deployable features` and click `Next >`
* Check the `trioeditor-feature`, select `Archive file`, choose an appropriate location,
and name the file `TrioEditor_1.0.6`, replacing `1.0.6` with the relevant version number.
    * In `Options`, make sure the `Generate p2 repository` is selected.
* Click `Finish`

Upload the generated `.zip` file to the tagged release commit in the repository.