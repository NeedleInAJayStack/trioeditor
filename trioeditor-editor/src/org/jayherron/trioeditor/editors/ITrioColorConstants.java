package org.jayherron.trioeditor.editors;

import org.eclipse.swt.graphics.RGB;

/**
 * The colors to highlight the text with. These are intended to match the SkySpark Func app axon
 * editor by default.
 * 
 * @author jay
 *
 */
public interface ITrioColorConstants {
	// Trio feature colors
	RGB DEFAULT = new RGB(0, 0, 0);
	RGB SPLIT = new RGB(0, 0, 127);
	RGB TAG = new RGB(127, 127, 0);
	
	// Axon code colors (to match the SkySpark Func app)
	// These may fill in non-axon areas of trio files.
	RGB BRACKET = new RGB(255, 0, 0);
	RGB COMMENT = new RGB(0, 127, 0);
	RGB COORDINATE = new RGB(127, 100, 0);
	RGB KEYWORD = new RGB(0, 0, 255);
	RGB NUMBER = new RGB(127, 0, 0);
	RGB REF = new RGB(127, 127, 127);
	RGB STRING = new RGB(0, 127, 127);
	RGB URI = new RGB(0, 127, 127); // Same as string
}
