
package org.jayherron.trioeditor.editors;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;


/**
 * An extension of WordRule that defines word rules
 */
public class KeywordRule extends WordRule {

	/**
	 * Creates a rule which will return the token associated with the detected word. 
	 * If no token has been associated, the scanner will be rolled back and an undefined 
	 * token will be returned in order to allow any subsequent rules to analyze the characters.
	 *
	 * @see #addWord(String, IToken)
	 */
	public KeywordRule() {
		this(Token.UNDEFINED, false);
	}

	/**
	 * Creates a rule which will return the token associated with the detected word. 
	 * If no token has been associated, the specified default token will be returned.
	 *
	 * @param defaultToken the default token to be returned on success
	 *			if nothing else is specified, may not be <code>null</code>
	 * @see #addWord(String, IToken)
	 */
	public KeywordRule(IToken defaultToken) {
		this(defaultToken, false);
	}

	/**
	 * Creates a rule which will return the token associated with the detected word. 
	 * If no token has been associated, the specified default token will be returned.
	 *
	 * @param defaultToken the default token to be returned on success
	 *			if nothing else is specified, may not be <code>null</code>
	 * @param ignoreCase the case sensitivity associated with this rule
	 * @see #addWord(String, IToken)
	 * @since 3.3
	 */
	public KeywordRule(IToken defaultToken, boolean ignoreCase){
		super(new IWordDetector() { 
			@Override
			public boolean isWordStart(char c) {
				return c != ' ' && Character.isJavaIdentifierStart(c);
			}

			@Override
			public boolean isWordPart(char c) {
				return Character.isJavaIdentifierPart(c);
			}
		}, defaultToken, false);
	}

	/**
	 * This overridden function requires that the previous character is not an allowed tag character(letter, 
	 * digit, or underscore) or a '.', indicating a function call.
	 * 
	 */
	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		scanner.unread(); 
		int p = scanner.read();
		if(!Character.isLetterOrDigit(p) && p != '_' && p != '.'){
			return super.evaluate(scanner);
		}
		return Token.UNDEFINED;
	}

}