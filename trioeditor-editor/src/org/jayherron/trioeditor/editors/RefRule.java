package org.jayherron.trioeditor.editors;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.text.rules.*;


/**
 * An implementation of <code>IRule</code> detecting a haystack Ref format, which is an "@", followed
 * by alphanumerical characters, underscores, dashes, and colons.
 */
public class RefRule implements IRule {

	/** Internal setting for the un-initialized column constraint */
	protected static final int UNDEFINED= -1;
	/** The token to be returned when this rule is successful */
	protected IToken fToken;

	/**
	 * Creates a rule which will return the specified
	 * token when a numerical sequence is detected.
	 *
	 * @param token the token to be returned
	 */
	public RefRule(IToken token) {
		Assert.isNotNull(token);
		fToken= token;
	}

	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		int c= scanner.read();
		if (c == '@') {
			do {
				c= scanner.read();
			} while (Character.isLetterOrDigit(c) || c=='_' || c=='-' || c==':');
			scanner.unread();
			return fToken;
		}

		scanner.unread();
		return Token.UNDEFINED;
	}
}
