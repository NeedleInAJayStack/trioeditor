package org.jayherron.trioeditor.editors;

import org.eclipse.jface.text.rules.IWhitespaceDetector;

/**
 * Detects whether a given character is whitespace. This is from the default
 * editor plugin stub, and is not currently used.
 * 
 * @author jay
 *
 */
public class TrioWhitespaceDetector implements IWhitespaceDetector {

	@Override
	public boolean isWhitespace(char c) {
		return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
	}
}
