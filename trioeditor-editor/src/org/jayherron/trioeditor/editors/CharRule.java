package org.jayherron.trioeditor.editors;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.text.rules.*;

/**
 * This is a rule that simply detects the input character in any context.
 * 
 * @author jay
 *
 */
public class CharRule implements IRule {
	
	/** Internal setting for the un-initialized column constraint */
	protected static final int UNDEFINED= -1;
	/** The token to be returned when this rule is successful */
	protected IToken fToken;
	/** The character to match */
	protected char character;

	/**
	 * Creates a rule which will return the specified
	 * token when a numerical sequence is detected.
	 *
	 * @param token the token to be returned
	 */
	public CharRule(char c, IToken token) {
		character = c;
		Assert.isNotNull(token);
		fToken= token;
	}

	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		int c= scanner.read();
		if (c == character) {
			return fToken;
		}

		scanner.unread();
		return Token.UNDEFINED;
	}
}
