package org.jayherron.trioeditor.editors;

import org.eclipse.jface.text.source.DefaultCharacterPairMatcher;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.SourceViewerDecorationSupport;

/**
 * This defines the editor and collects different aspects like configuration, document provider, etc
 * 
 * @author jay
 *
 */
public class TrioEditor extends TextEditor {

	private ColorManager colorManager;
	
    private final static char[] PAIRS = {'(',')','{','}','[',']'};
    private DefaultCharacterPairMatcher pairsMatcher = new DefaultCharacterPairMatcher(PAIRS);
    
	public TrioEditor() {
		super(); // Creates the TextEditor preference store.
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new TrioConfiguration(colorManager, getPreferenceStore()));
		setDocumentProvider(new TrioDocumentProvider());
	}
	@Override
	public void dispose() {
		colorManager.dispose();
        pairsMatcher.dispose();
		super.dispose();
	}
	
	@Override
    protected void configureSourceViewerDecorationSupport(SourceViewerDecorationSupport support) {
        support.setCharacterPairMatcher(pairsMatcher);
        // We must set up a preference page to support bracket-matching, because the default text editor does not...
        //support.setMatchingCharacterPainterPreferenceKeys(JavaPreferences.EDITOR_MATCHING_BRACKETS,
        //        JsonPreferences.EDITOR_MATCHING_BRACKETS_COLOR);
        super.configureSourceViewerDecorationSupport(support);
    }

}
