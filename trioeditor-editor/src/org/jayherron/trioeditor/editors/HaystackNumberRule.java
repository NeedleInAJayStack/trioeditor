package org.jayherron.trioeditor.editors;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.text.rules.*;


/**
 * An implementation of <code>IRule</code> detecting a numerical value with optional unit. Units are
 * allowed to have an underscore, but special characters like subscipts or superscripts are missed.
 */
public class HaystackNumberRule implements IRule {

	/** Internal setting for the un-initialized column constraint */
	protected static final int UNDEFINED= -1;
	/** The token to be returned when this rule is successful */
	protected IToken fToken;

	/**
	 * Creates a rule which will return the specified
	 * token when a numerical sequence is detected.
	 *
	 * @param token the token to be returned
	 */
	public HaystackNumberRule(IToken token) {
		Assert.isNotNull(token);
		fToken= token;
	}

	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		// Get the preceeding character
		scanner.unread();
		int p= scanner.read(); 
		if(!Character.isLetter(p)) { // Only continue if prev character is not a letter. 
			int c= scanner.read(); 
			if(Character.isDigit(c)) {
				do {
					c= scanner.read();
				} while (Character.isLetterOrDigit(c) || c=='_'); // Misses exponent characters.
				scanner.unread();
				return fToken;
			}
			scanner.unread();
			return Token.UNDEFINED;
		}
		return Token.UNDEFINED;
	}
}
