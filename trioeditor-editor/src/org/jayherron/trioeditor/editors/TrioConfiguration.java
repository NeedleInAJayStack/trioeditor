package org.jayherron.trioeditor.editors;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.editors.text.TextSourceViewerConfiguration;

/**
 * Class that configures the editor and scanner. By extending TextSourceViewerConfiguration,
 * we respect all the default text editor preferences.
 * 
 * @author jay
 *
 */
public class TrioConfiguration extends TextSourceViewerConfiguration {
	private TrioScanner scanner;
	private ColorManager colorManager;

	public TrioConfiguration(ColorManager colorManager, IPreferenceStore prefStore) {
		super(prefStore); // This makes the TrioEditor respond correctly to the TextEditor preferences.
		this.colorManager = colorManager;
		
		scanner = new TrioScanner(colorManager);
		IToken defaultToken = new Token(new TextAttribute(colorManager.getColor(ITrioColorConstants.DEFAULT)));
		scanner.setDefaultReturnToken(defaultToken);
	}

	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] {
			IDocument.DEFAULT_CONTENT_TYPE,
			TrioPartitionScanner.TRIO_COMMENT,
			TrioPartitionScanner.TRIO_STRING
		};
	}

	protected TrioScanner getScanner() {
		return scanner;
	}

	@Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		PresentationReconciler reconciler = new PresentationReconciler();
		
		// Set how we deal with default content partitions
		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(getScanner());
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);
		
		// Set how we deal with comment partitions
		TextAttribute comment = new TextAttribute(colorManager.getColor(ITrioColorConstants.COMMENT));
		NonRuleBasedDamagerRepairer commentDr = new NonRuleBasedDamagerRepairer(comment);
		reconciler.setDamager(commentDr, TrioPartitionScanner.TRIO_COMMENT);
		reconciler.setRepairer(commentDr, TrioPartitionScanner.TRIO_COMMENT);
		
		// Set how we deal with string partitions
		TextAttribute string = new TextAttribute(colorManager.getColor(ITrioColorConstants.STRING));
		NonRuleBasedDamagerRepairer stringDr = new NonRuleBasedDamagerRepairer(string);
		reconciler.setDamager(stringDr, TrioPartitionScanner.TRIO_STRING);
		reconciler.setRepairer(stringDr, TrioPartitionScanner.TRIO_STRING);
		
		return reconciler;
	}

}