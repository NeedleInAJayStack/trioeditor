package org.jayherron.trioeditor.editors;

import org.eclipse.jface.text.rules.*;
import org.eclipse.jface.text.*;

/**
 * Defines the rules by which the Trio editor will operate.
 * 
 * @author jay
 *
 */
public class TrioScanner extends RuleBasedScanner {
	
	public TrioScanner(ColorManager manager) {
		IToken comment = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.COMMENT)));
		IToken split = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.SPLIT)));
		IToken string = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.STRING)));
		IToken uri = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.URI)));
		IToken coordinate = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.COORDINATE)));
		IToken number = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.NUMBER)));
		IToken ref = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.REF)));
		IToken keyword = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.KEYWORD)));
		IToken bracket = new Token(new TextAttribute(manager.getColor(ITrioColorConstants.BRACKET)));
		
		IRule[] rules = new IRule[15];
		
		// End-of-line rules
		// Comment
		rules[0] = new EndOfLineRule("//", comment, '\\');
		
		// Single-line rules
		// Rec split
		rules[1] = new SingleLineRule("---", "", split);
		// String
		rules[2] = new SingleLineRule("\"", "\"", string, '\\');
		// Uri
		rules[3] = new SingleLineRule("`", "`", uri, '\\');
		// Coordinates
		rules[4] = new SingleLineRule("C(", ")", coordinate);
		
		// Numbers
		rules[5] = new HaystackNumberRule(number);
		// Refs
		rules[6] = new RefRule(ref);
		
		// Add generic whitespace rule.
		rules[7] = new WhitespaceRule(new TrioWhitespaceDetector());
		
		// Keywords
		KeywordRule keywords =  new KeywordRule();
		// Axon keywords
		keywords.addWord("return", keyword);
		keywords.addWord("if", keyword);
		keywords.addWord("else", keyword);
		keywords.addWord("do", keyword);
		keywords.addWord("end", keyword);
		keywords.addWord("and", keyword);
		keywords.addWord("or", keyword);
		keywords.addWord("try", keyword);
		keywords.addWord("catch", keyword);
		keywords.addWord("throw", keyword);
		keywords.addWord("null", keyword);
		keywords.addWord("true", keyword);
		keywords.addWord("false", keyword);
		rules[8] = keywords;
		
		// Character rules
		rules[9] =  new CharRule('(', bracket);
		rules[10] =  new CharRule(')', bracket);
		rules[11] =  new CharRule('{', bracket);
		rules[12] =  new CharRule('}', bracket);
		rules[13] =  new CharRule('[', bracket);
		rules[14] =  new CharRule(']', bracket);
				
		setRules(rules);
	}
}
