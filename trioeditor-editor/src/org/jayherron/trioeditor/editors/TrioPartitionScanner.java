package org.jayherron.trioeditor.editors;

import org.eclipse.jface.text.rules.*;

/**
 * The parition scanner allows us to define different functionality in
 * different parts of the file.
 * 
 * We must define multi-line rules as unique partitions because otherwise 
 * it will break the damage/repair process for the line that was edited.
 * This is because the default damage/repairer works on a line-by-line basis.
 * Instead, we must partition each multi-line format and force it to just 
 * return the correct text attribute.
 * 
 * @author jay
 *
 */
public class TrioPartitionScanner extends RuleBasedPartitionScanner {
	public final static String TRIO_COMMENT = "__trio_comment";
	public final static String TRIO_STRING = "__trio_string";

	public TrioPartitionScanner() {

		IToken comment = new Token(TRIO_COMMENT);
		IToken string = new Token(TRIO_STRING);

		IPredicateRule[] rules = new IPredicateRule[2];
		
		// Multiline-comment
		// Comment
		rules[0] = new MultiLineRule("/*", "*/", comment);
		// String: triple-quoted
		rules[1] = new MultiLineRule("\"\"\"", "\"\"\"", string);
		
		setPredicateRules(rules);
	}
}
