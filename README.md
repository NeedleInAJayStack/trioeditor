# Trio Editor

This is an plugin for [Elipse](https://www.eclipse.org/eclipseide/) or 
[F4](https://github.com/xored/f4) that adds a Trio file editor. 
[Trio](https://project-haystack.org/doc/Trio) is a file format defined by 
[Project Haystack](https://project-haystack.org/). Since Trio files are commonly
used to store Axon code for the [SkySpark](https://skyfoundry.com/product) product,
there is added support for Axon syntax highlighting that closely matches the Code 
IDE in the SkySpark platform.

## Installation

* Download the .zip file from the most recent 
[release tag](https://gitlab.com/NeedleInAJayStack/trioeditor/tags).
* Open Eclipse or F4, and from the menu bar click `Help` -> `Install new software...`
* Drag and drop the downloaded .zip file into the dialogue window.
    * You may need to un-check the `Group items by category` box to see the plugin
    * If the drage an drop doesn't work, you may need to click `Add...` -> 
    `Archive...` and then select the downloaded .zip file.
* Click the checkbox next to the `Trio Editor Plugin`
* Click `Next >` and follow on screen instructions

After Eclipse or F4 restarts, opening `.trio` files should present them with the 
syntax highlighed. You can force a file to open with the trio editor by right-clicking 
and selecting `Open with...` -> `Other...` -> `Trio Editor`

## Contributing

Contributions are welcome! Instructions on how to configure and build the plugin
are included in the `contributing.md` file.

## Support

If you experience a bug, feel free to create a GitLab issue in this repository or
contact Jay Herron via NeedleInAJayStack at protonmail.com with any questions.

## Potential future improvements

* Insert package onto Eclipse Marketplace
* Bracket matching for all brackets
* Preferences page to customize syntax colors
* Complete syntax highlighting with additional trio features, including:
    * tags
    * unquoted single-line strings
* Trio format validation
* Auto-formatter for Trio files
* Axon language enhancements, including:
    * Coloring of variables
    * Linking of variables
    * Expand/contract code based on presence of do/end
    * Validation
    * Outline view
